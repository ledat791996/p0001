import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {User} from "./entities/user.entity";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class UserService extends TypeOrmCrudService<User>{
  constructor(@InjectRepository(User) rebo) {
    super(rebo);
  }

  public getUserByUserName(userName: string): Promise<User> {
    return this.findOne({where: {userName:userName}});
  }
}
