import { Role } from "@core/enum";
import { UniqueUser } from "@core/validator/unique-user";
import {IsEnum, IsNotEmpty, IsString, MinLength, Validate} from "class-validator";
import { User } from "../entities/user.entity";

export class CreateUserDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(6)
    @Validate(UniqueUser)
    userName: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(6)
    password: string;

    @IsNotEmpty()
    @IsEnum(Role)
    role: Role;
}
