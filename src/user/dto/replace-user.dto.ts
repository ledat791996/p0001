import {IsString, MinLength} from "class-validator";

export class ReplaceUserDto {
    @IsString({})
    @MinLength(6)
    userName: string

    @IsString()
    @MinLength(6)
    password: string
}
