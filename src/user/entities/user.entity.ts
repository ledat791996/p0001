import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import * as bcrypt from 'bcrypt';
import { Role } from "@core/enum";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column({name: 'user_name', unique: true})
    userName: string

    @Column({name:'password'})
    password: string;

    @Column({
        name:'is_active',
        default: true
    })
    isActive: boolean;

    @Column({
        type: 'enum',
        enum: Role,
    })
    role: Role;

    @Column({
        name: 'created_at',
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: string;

    @Column({
        name: 'updated_at',
        type: 'datetime',
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updatedAt?: Date;




    @BeforeInsert()
    @BeforeUpdate()
    async updatePassword() {
        if (this.password) {
            const salt = await bcrypt.genSalt();
            this.password = await bcrypt.hash(this.password, salt);
        }
    }
}
