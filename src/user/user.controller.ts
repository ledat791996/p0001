import { Controller, Get} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {Crud} from "@nestjsx/crud";
import {User} from "./entities/user.entity";
import {ReplaceUserDto} from "./dto/replace-user.dto";
import { Role } from '@core/enum';

@Crud({
  model: {
    type: User
  },
  dto: {
    create: CreateUserDto,
    replace: ReplaceUserDto,
    update:UpdateUserDto,
  },
  routes: {
    only: ["getOneBase", "updateOneBase", "createOneBase", "getManyBase", "replaceOneBase"],
  },
  query: {
    exclude: ["password"]
  }
})
@Controller('user')
export class UserController {
  constructor(public service: UserService) {}

  @Get('role')
  getTopic() {
    console.log('role: ', Role);
    const role =  Role;
    for (const [key, value] of Object.entries(role)) {
      if(typeof value == "number") {
        delete role[key];
      }
    }
    return role;
  }
}
