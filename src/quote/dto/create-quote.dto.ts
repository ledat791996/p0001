import { Topic } from "@core/enum";
import {IsEmail, IsEnum, IsNotEmpty, IsString} from "class-validator";

export class CreateQuoteDto {
    @IsString()
    @IsNotEmpty()
    fullName: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    phone: string;

    @IsNotEmpty()
    @IsEnum(Topic)
    topic: Topic;
}
