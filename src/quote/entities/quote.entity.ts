
import { Topic } from "@core/enum";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Quote {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({name: 'full_name'})
    fullName: string;

    @Column()
    email: string;

    @Column()
    phone: string;

    @Column({
        type: 'enum',
        enum: Topic,
    })
    topic: Topic;
}

