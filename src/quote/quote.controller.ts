import { PublicAPI } from '@core/custom-decorator';
import { Topic } from '@core/enum';
import { Controller, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { CreateQuoteDto } from './dto/create-quote.dto';
import { Quote } from './entities/quote.entity';
import { QuoteService } from './quote.service';

@Controller('quote')
@Crud({
  model: {
    type: Quote
  },
  routes: {
    only: ["createOneBase", "getManyBase", "getOneBase"],
    createOneBase: {
      decorators: [PublicAPI()]
    }
  },
  dto: {
    create: CreateQuoteDto
  }
})
export class QuoteController {
  constructor(public service: QuoteService) {}

  @PublicAPI()
  @Get('topic')
  getTopic() {
    return Object.values(Topic);
  }
}
