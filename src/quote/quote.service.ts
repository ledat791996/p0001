import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Quote } from './entities/quote.entity';

@Injectable()
export class QuoteService extends TypeOrmCrudService<Quote>{
constructor(@InjectRepository(Quote) rebo) {
    super(rebo);
}
}  