export enum Topic {
    SOLE_PROPRIETORSHIP = "Sole Proprietorship",
    PARTNERSHIP = "Partnership",
    CORPORATION = "Corporation",
}

export enum Role {
    Admin = "Admin",
    User = "User"
}