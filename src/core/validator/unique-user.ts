import { Injectable } from '@nestjs/common';
import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { UserService } from 'src/user/user.service';

@ValidatorConstraint({ name: 'uniqueUser', async: false })
@Injectable()
export class UniqueUser implements ValidatorConstraintInterface {

  constructor(
    protected readonly userService: UserService,
  ) {}

  public async validate<E>(value: string, args: any) {
    return !await this.userService.getUserByUserName(value);
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'user name $value đã tồn tại';
  }
}