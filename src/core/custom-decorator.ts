
import { SetMetadata } from '@nestjs/common';

export const PUBLIC_API = 'isPublic';
export const PublicAPI = () => SetMetadata(PUBLIC_API, true);