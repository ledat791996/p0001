import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ContactUs } from './entities/contact-us.entity';

@Injectable()
export class ContactUsService extends TypeOrmCrudService<ContactUs>{
  constructor(@InjectRepository(ContactUs) rebo) {
    super(rebo);
  }
}
