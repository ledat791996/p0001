import { PublicAPI } from '@core/custom-decorator';
import { Controller } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { ContactUsService } from './contact-us.service';
import { CreateContactUsDto } from './dto/create-contact-us.dto';
import { ContactUs } from './entities/contact-us.entity';

@Crud({
  model: {
    type: ContactUs
  },
  routes: {
    only: ["getOneBase", "createOneBase", "getManyBase"],
    createOneBase: {
      decorators: [PublicAPI()]
    }
  },
  dto: {
    create: CreateContactUsDto
  }
})
@Controller('contact-us')
export class ContactUsController {
  constructor(public service: ContactUsService) {}
}
