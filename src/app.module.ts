import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuoteModule } from './quote/quote.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from 'src/authentication/guard/jwt-auth.guard';
import { ContactUsModule } from './contact-us/contact-us.module';

@Module({
  imports: [
      UserModule,
      TypeOrmModule.forRoot(),
      QuoteModule,
      AuthenticationModule,
      ContactUsModule
  ],
  controllers: [AppController],
  providers: [AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
