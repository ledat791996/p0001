FROM node:16.10.0

RUN apt update && apt install -y vim systemd
RUN rm -rf /etc/localtime
RUN ln -s /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
RUN yarn global add @nestjs/cli
USER node
WORKDIR /usr/src/app


